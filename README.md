# uButton

A MicroPython library for controlling reading and debouncing pushbutton inputs, including "short" and "long" press callbacks, using `uasyncio`. 

Avoids using a `Timer` instance, to prevent any potential conflicts with other modules that may be using it. 


## Installation

Installing is easy - just copy the `ubutton.py` file to your MicroPython board. It can live either in the root, or in a `/lib` folder. You will also need `uasyncio` installed to the board. 

The file is only a few kilobytes, but for more efficiency it can be frozen into a MicroPython binary image - just place the file inside the `ports/<board>/modules` folder when building MicroPython from source, then flash to the board as usual. 


## Usage

Usage is simple enough, create an instance of `uButton` like so:

```python
button = uButton(
    machine.Pin(25, machine.Pin.IN),
    cb_short = lambda: print('short press'),
    short_wait=True,
    cb_long = lambda: print('long press'),
    bounce_time=25,
    long_time=500,
    act_low=True
)
```

Any callable can be given as the `cb_short` and `cb_long` functions, or they can be left out if not needed. Setting `short_wait` to `True` will cause the `cb_short` callback to only be called once the button has been released, to prevent both `cb_short` and `cb_long` being called when a long press occurs. 

By default, `uButton` assumes the GPIO attached to the button is pulled up externally and that pressing the button will short the input to ground (i.e. the button is active-low). It is possible to do the opposite (i.e. active-high) by changing the optional keyword argument `act_low` to `False`. 

Then when setting up the event loop, add the `run()` coroutine as a task:

```python
# Get a reference to the event loop
loop = asyncio.get_event_loop()
# Schedule coroutines to run ASAP
loop.create_task(asyncio.coroutine(button.run()))
```

Then start the loop as required for the rest of your code, probably with `loop.run_forever()`.

See the example in [`examples/main.py`](./example/main.py) for a demo. 


## License

uButton, A MicroPython library for reading and debouncing pushbutton inputs

Copyright (C) 2019, Sean Lanigan

uButton is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

uButton is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License along with uButton.  If not, see <https://www.gnu.org/licenses/>.

See [LICENSE](./LICENSE) for the full text. 
